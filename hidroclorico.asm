          list =16f873

          #include <p16f873>

          TICS       equ      0x20
          SEG3       equ      0x21
          SEG5       equ      0X22
          FLAGS      equ      0x23
          TEMP_W     equ      0x24
          TEMP_ST    equ      0X25
          
                org 0
                goto inicio
                org 4
                goto rtc

        inicio
                clrf        PORTB
                clrf        INTCON      ;inhabilitar todas las interruociones
                bsf         STATUS, rp0 ;seleccionar banco1
                movfw       0xC4        ;pre-divisor de 32 asignado
                movfw       OPTION_REG  ;AL tIMER0.
                clrf        TRISB       ;Puerto B en salida.
                bcf         STATUS, RP0 ;Seleccionar banco 0.
                movfw       .0          ;Modulo de coteo de 256
                movwf       TMR0        ;en el timer0.
                movfw       .122        ;cantidad de tics por seg.
                movwf       TICS        ;

